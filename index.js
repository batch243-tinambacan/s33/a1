// console.log("Hi Activity")


// Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.

const listArray = [];
    fetch("https://jsonplaceholder.typicode.com/todos")
    .then((response) => response.json())
    .then((data) => {
        data.map((elem) => {
            listArray.push(elem.title);
        });
    });
    console.log(listArray)
    
// Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.

	    fetch('https://jsonplaceholder.typicode.com/todos/1')
	    .then((response)=>response.json())
	    .then((json)=> console.log(json))


// Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.
	
		fetch('https://jsonplaceholder.typicode.com/todos', {

			// Sets the method to POST
			method: 'POST',

			// Specified that the content will be in  a JSON structure
			headers: {
				'Content-Type' : 'application/json'
			},

			body: JSON.stringify({
				title: "Created to do list item",
				completed: false,
			})
		})
		.then((response)=>response.json())
		.then((json)=> console.log(json))

// Update a to do list item by changing the data structure to contain the following properties:


		fetch('https://jsonplaceholder.typicode.com/todos/1', {
			method: 'PUT',
			headers : {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				title: 'Update to do list item',
				description: "To update my to do list with a different data structure",
				status: "Pending",
				dateCompleted: "Pending",
				userId: 1
			})
		})
		.then((response)=>response.json())
		.then((json)=> console.log(json))

// Update a to do list item by changing the status to complete and add a date when the status was changed.

		fetch('https://jsonplaceholder.typicode.com/todos/1', {
			method: 'PATCH',
			headers : {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				dateCompleted: "07/09/21",
				status: "Completed"
			})
		})
		.then((response)=>response.json())
		.then((json)=> console.log(json))
